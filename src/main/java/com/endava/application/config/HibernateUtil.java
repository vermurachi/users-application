package com.endava.application.config;

import com.endava.application.root.Department;
import com.endava.application.root.Role;
import com.endava.application.root.User;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import static java.util.Objects.isNull;


@Slf4j
public class HibernateUtil {

    private static SessionFactory sessionFactory;

    static {
        try {
            loadSessionFactory();
        } catch (Exception e) {
            log.error("Exception while initializing hibernate util");
            e.printStackTrace();
        }
    }


    public static Session getSession() throws HibernateException {
        Session session = sessionFactory.openSession();

        if (isNull(session))
            sessionFactory = loadSessionFactory();

        return session;
    }

    private static SessionFactory loadSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Role.class);
        configuration.addAnnotatedClass(Department.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }
}
