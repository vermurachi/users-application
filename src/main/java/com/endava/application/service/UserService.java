package com.endava.application.service;

import com.endava.application.root.User;

import java.sql.SQLException;
import java.util.List;

public interface UserService {
    User createUser(User user) throws SQLException;

    List<User> getAllUsers() throws SQLException;

    User getUserById(int id) throws SQLException;

    User updateUser(int id, User user) throws SQLException;

    User deleteUser(int id) throws SQLException;
}
