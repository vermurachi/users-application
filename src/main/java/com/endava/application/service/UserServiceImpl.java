package com.endava.application.service;

import com.endava.application.dao.UserDao;
import com.endava.application.root.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.isNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    @Override
    public User createUser(User user) throws SQLException {
        return userDao.createUser(user);
    }

    @Override
    public List<User> getAllUsers() throws SQLException {
        List<User> userList = userDao.getAllUsers();

        if (userList.isEmpty()) {
            log.info("The user list is empty!");
        }

        return userList;
    }

    public User getUserById(int id) throws SQLException {

        return userDao.getUserById(id);
    }

    @Override
    public User updateUser(int id, User user) throws SQLException {
        User userToUpdate = getUserById(id);

        if (isNull(userToUpdate)) {
            log.info("User with id " + id + " is not in the list!");
        }

        userToUpdate.setFirstName(user.getFirstName());
        userToUpdate.setLastName(user.getLastName());
        userToUpdate.setBirthDay(user.getBirthDay());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setRole(user.getRole());
        userToUpdate.setDepartment(user.getDepartment());

        return userDao.updateUser(userToUpdate);
    }

    @Override
    public User deleteUser(int id) {
        User deletedUser = userDao.deleteUser(id);

        if (isNull(deletedUser)) {
            log.info("User with id " + id + " is not in the list!");
        }

        return deletedUser;
    }
}
