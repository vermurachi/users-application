package com.endava.application.controller;

import com.endava.application.root.Department;
import com.endava.application.root.Role;
import com.endava.application.root.User;
import com.endava.application.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import static java.util.Objects.isNull;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final Scanner scanner;

    public void start() throws SQLException {
        while (true) {
            displayMenu();
            int option = scanner.nextInt();
            switch (option) {
                case 1:
                    createUser();
                    break;
                case 2:
                    updateUser();
                    break;
                case 3:
                    findAll();
                    break;
                case 4:
                    findByID();
                    break;
                case 5:
                    deleteUser();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Unknown operation! Please select from above!");
            }
        }
    }

    private void createUser() throws SQLException {
        User createdUser = new User();
        Role createdRole = new Role();
        Department createdDepartment = new Department();

        System.out.println("Insert first name: ");
        String firstName = scanner.next();
        createdUser.setFirstName(firstName);

        System.out.println("Insert last name: ");
        String lastName = scanner.next();
        createdUser.setLastName(lastName);

        System.out.println("Insert date of birth: ");
        String birthDay = scanner.next();
        createdUser.setBirthDay(birthDay);

        System.out.println("Insert email address: ");
        String email = scanner.next();
        createdUser.setEmail(email);

        System.out.println("Insert role: ");
        String role = scanner.next();
        createdRole.setRoleName(role);
        createdUser.setRole(createdRole);

        System.out.println("Insert department: ");
        String department = scanner.next();
        createdDepartment.setDepartmentName(department);
        createdUser.setDepartment(createdDepartment);

        userService.createUser(createdUser);
        System.out.println("User successfully created!");
    }

    private void deleteUser() throws SQLException {
        System.out.println("Insert ID of the user you want to delete: ");
        int id = Integer.parseInt(scanner.next());
        User deletedUser = userService.deleteUser(id);
        System.out.println("Deleted user: " + deletedUser);
    }

    private void findByID() throws SQLException {
        System.out.println("Insert ID of the user you want to find: ");
        int id = Integer.parseInt(scanner.next());
        User user = userService.getUserById(id);
        if (isNull(user)) {
            System.out.println("User with " + id + " is not in the list!");
        }
        System.out.println(user);
    }

    private void findAll() throws SQLException {
        System.out.println("All users: ");
        List<User> allUsers = userService.getAllUsers();
        for (User user : allUsers) {
            System.out.println(user);
        }
    }

    private void updateUser() throws SQLException {
        System.out.println("Insert ID of the user you want to update: ");
        int id = Integer.parseInt(scanner.next());

        User updateUser = new User();
        Role updateRole = new Role();
        Department updateDepartment = new Department();

        System.out.println("Insert first name: ");
        String firstName = scanner.next();
        updateUser.setFirstName(firstName);

        System.out.println("Insert last name: ");
        String lastName = scanner.next();
        updateUser.setLastName(lastName);

        System.out.println("Insert date of birth: ");
        String birthDay = scanner.next();
        updateUser.setBirthDay(birthDay);

        System.out.println("Insert email address: ");
        String email = scanner.next();
        updateUser.setEmail(email);

        System.out.println("Insert role: ");
        String role = scanner.next();
        updateRole.setRoleName(role);
        updateUser.setRole(updateRole);

        System.out.println("Insert department: ");
        String department = scanner.next();
        updateDepartment.setDepartmentName(department);
        updateUser.setDepartment(updateDepartment);

        userService.updateUser(id, updateUser);
        System.out.println("User with id " + id + " successfully updated!");
    }

    private void displayMenu() {
        System.out.println("1. Create user");
        System.out.println("2. Update user");
        System.out.println("3. Display all users");
        System.out.println("4. Find user");
        System.out.println("5. Delete user");
        System.out.println("0. Exit");
        System.out.println("Please select an option: ");
    }
}
