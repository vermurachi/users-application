package com.endava.application.dao;

import com.endava.application.config.HibernateUtil;
import com.endava.application.root.User;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
public class UserDaoImpl implements UserDao {

    private Transaction transaction;
    private Session session;

    @Override
    public User createUser(User user) {
        session = HibernateUtil.getSession();
        transaction = session.beginTransaction();

        session.save(user);
        session.saveOrUpdate(user.getRole());
        session.saveOrUpdate(user.getDepartment());

        transaction.commit();
        session.close();

        return user;
    }


    public List<User> getAllUsers() {
        session = HibernateUtil.getSession();

        List<User> allUsers = session.createQuery("from User", User.class).getResultList();

        session.close();

        return allUsers;
    }

    @Override
    public User getUserById(int id) {
        session = HibernateUtil.getSession();

        User foundUser = session.get(User.class, id);

        session.close();

        return foundUser;
    }

    @Override
    public User updateUser(User user) {
        session = HibernateUtil.getSession();
        transaction = session.beginTransaction();

        session.saveOrUpdate(user.getRole());
        session.saveOrUpdate(user.getDepartment());
        session.update(user);

        transaction.commit();
        session.close();

        return user;
    }

    @Override
    public User deleteUser(int id) {
        session = HibernateUtil.getSession();
        transaction = session.beginTransaction();

        User userToDelete = session.get(User.class, id);
        session.delete(userToDelete);

        transaction.commit();
        session.close();

        return userToDelete;
    }
}
