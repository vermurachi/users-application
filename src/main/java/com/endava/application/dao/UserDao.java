package com.endava.application.dao;

import com.endava.application.root.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {

    User createUser(User user) throws SQLException;

    List<User> getAllUsers() throws SQLException;

    User getUserById(int id) throws SQLException;

    User updateUser(User user);

    User deleteUser(int id);
}
