CREATE TABLE IF NOT EXISTS users (
    id SERIAL,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    email VARCHAR(255),
    birth_day VARCHAR(255),
    role_id INT,
    department_id INT,
    CONSTRAINT user_pk PRIMARY KEY (id)
);
