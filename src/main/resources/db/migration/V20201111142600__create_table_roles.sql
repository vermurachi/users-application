CREATE TABLE IF NOT EXISTS roles (
    id SERIAL,
    role_name VARCHAR(255),
    PRIMARY KEY (id)
);

ALTER TABLE users ADD CONSTRAINT users_role
    FOREIGN KEY (role_id)
    REFERENCES roles (id);
