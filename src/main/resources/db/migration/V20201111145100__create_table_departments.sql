CREATE TABLE IF NOT EXISTS departments (
    id SERIAL,
    department_name VARCHAR(255),
    user_id INT,
    PRIMARY KEY (id)
);

ALTER TABLE users ADD CONSTRAINT users_department
    FOREIGN KEY (department_id)
    REFERENCES departments (id);
